function audio = subtract_filter(option)
% Loading files
load Male2_speech.mat, load Mic1.mat, load Mic2.mat;


% Naming variables
mic1 = Mic1;
mic2 = Mic2;
delator_voice = (y(1:(length(mic1)))).';
% delator_voice = (delator_voice(1:(length(mic1)))).';

clear y, clear Mic1, clear Mic2;

%--------------------------------------------------------------------------
% 1 - Excluding delator voice
%--------------------------------------------------------------------------
% 
% First, we'll correlate the known voice [delator_voice] with the captured
% audios [Mic1, Mic2] and then, take the correlation coefficient for each data
% to extract it from the captured ones.

% Calculating the covariance

sigma_d1 = sigma(mic1,delator_voice);

% Taked the correlation between the variables, we subtract from audio

delator_extracted_1 = mic1 - sigma_d1*delator_voice;

% Calculating the covariance

sigma_d2 = sigma(mic2,delator_voice);

% Taked the correlation between the variables, we subtract from audio

delator_extracted_2 = mic2 - sigma_d2*delator_voice;

signal_1 = delator_extracted_1;

signal_2 = delator_extracted_2;


%--------------------------------------------------------------------------
% 2 - Separating voices - Male
%--------------------------------------------------------------------------

% Calculating the covariance

sigma_d3 = sigma(mic2,mic1);

% Taked the correlation between the variables, we subtract from audio

noisy = signal_1 - sigma_d3*signal_2;
noisy = noisy - sigma_d3*signal_2;

sigma_d3 = sigma(signal_2.',noisy.');
noisy = noisy - sigma_d3*signal_2;

male = noisy;

%--------------------------------------------------------------------------
% 2 - Separating voices - Female
%--------------------------------------------------------------------------

sigma_d3 = sigma(signal_2.',noisy.');
noisy = noisy - sigma_d3*signal_2;

female = noisy;

switch option
    case 1
        audio = delator_extracted_1;
    case 2
        audio = delator_extracted_2;
    case 3 
        audio = female;
    case 4
        audio = male;
end

return
