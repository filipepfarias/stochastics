function [response_signal, response_frequency] = createFFTN(signal, Fs)

	% vari�vel N recebe o tamanho do vetor signal
	N = length(signal);

	% k � um vetor que vai de zero at� N menos 1
	k = 0:length(signal)-1;                     

	% Vetor de tempo N dividido pela frequ�ncia de amostragem     
	time = N/Fs;
	frequency = k/time;
	Fs = length(frequency);

	% signal recebe a FFT normalizada do vetor signal sobre N
	signal = fftn(signal)/N;   

	% cutOff ajusta o eixo signal    
	response_signal = abs(signal(1:ceil(N/2)));
	response_frequency = frequency(1:ceil(N/2));
end
