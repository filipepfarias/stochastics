Error using corr (line 102)
X and Y must have the same number of rows.
 
>> sigma = corr(microphone_1.',microphone_2.');
>> test = microphone_1 - sigma.*microphone_2;
>> sound(test)
>> test2 = microphone_2 - sigma.*microphone_1;
>> sound(test2)
>> sound(microphone_1)
>> clear sound
>> test = test - sigma.*microphone_1;
>> sound(test)
>> clear sound
>> test = microphone_1 - sigma.*microphone_2;
>> sound(test)
>> clear sound
>> sigma = corr(test.',microphone_2.');
>> test = test - sigma.*microphone_1;
>> sound(test)
>> clear sound
>> sigma = corr(test.',microphone_2.');
>> test = test - sigma.*microphone_1;
>> sound(test)
>> sigma = corr(test.',microphone_2.');
>> test = test - sigma.*microphone_1;
>> sound(test)
>> clear sound
>> sigma = corr(test.',microphone_1.');
>> test = test - sigma.*microphone_1;
>> sound(test)
>> clear sound
>> sigma = corr(test.',microphone_1.');
>> test = test - sigma.*microphone_1;
>> sound(test)
>> clear (test, test2, sigma)