% Sine signal
% Fs = 1000;
% t = 0:1/Fs:1-1/Fs;
%x_sine = sin(2*pi*150*t) + 2*sin(2*pi*200*t) + sin(2*pi*350*t);
% x_sine = sin(2*pi*150*t) + sin(2*pi*200*t) + sin(2*pi*350*t);

function dep(signal,Fs)

N = length(signal);
xdft = fft(signal);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)) * abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(signal):Fs/2;

% figure
     plot(freq,(psdx));
     hold on
%      legend(lgd);

end

