function signal = fir_filter(delator_extracted_1,lower_freq, upper_freq,Mic1)
Fs = 8000;
%--------------------------------------------------------------------------
% 2 - Separating other voices
%--------------------------------------------------------------------------
% 
% The principle idea is correlate the two microphones in order to take just
% the voices

% N = length(delator_voice);
% xdft = fft(delator_voice);
% xdft = xdft(1:N/2+1);
% psdx = (1/(Fs*N)) * abs(xdft).^2;
% psdx(2:end-1) = 2*psdx(2:end-1);
% figure;
% hold on;
% plot(psdx,'r');
% freq = 0:Fs/length(delator_voice):Fs/2;

% N_0 = length(mic1);
% xdft_0 = fft(mic1);
% xdft_0 = xdft_0(1:N/2+1);
% psdx_0 = (1/(Fs*N_0)) * abs(xdft_0).^2;
% psdx_0(2:end-1) = 2*psdx_0(2:end-1);
% figure;
% plot(psdx_0);
% freq = 0:Fs/length(mic1):Fs/2;

N_1 = length(delator_extracted_1);
xdft_1 = fft(delator_extracted_1);
xdft_1 = xdft_1(1:N_1/2+1);
psdx_1 = (1/(Fs*N_1)) * abs(xdft_1).^2;
psdx_1(2:end-1) = 2*psdx_1(2:end-1);

   signal=delator_extracted_1;
      dt = 1/Fs;
      N = length(signal);
      dF = Fs/N;
      f = (-Fs/2:dF:Fs/2-dF)';
     
      % Band-Pass Filter:
%       lower_freq = 100;
%       upper_freq = 900;
   
      BPF = ((lower_freq < abs(f)) & (abs(f) < upper_freq));
%       figure;
%       plot(f,BPF);
  
      time = dt*(0:N-1)';
%       figure;
      plot(time,signal);

   signal=signal-mean(signal);

      spektrum = fftshift(fft(signal))/N;
%       figure;
%       subplot(2,1,1);
      plot(f,abs(spektrum));

     spektrum = BPF.*spektrum.';
%      subplot(2,1,2);
     plot(f,abs(spektrum));

     signal=ifft(ifftshift(spektrum),'symmetric'); %inverse ifft
     
%      sigma = corr(signal,Mic1.');
%      signal = signal.*sigma;
     figure;
     plot(signal);

% figure;
% plot(psdx_1);
% freq_1 = 0:Fs/length(delator_extracted_1):Fs/2;
end