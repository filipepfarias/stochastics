   signal=delator_extracted_1;
      dt = 1/Fs;
      N = length(signal);
      dF = Fs/N;
      f = (-Fs/2:dF:Fs/2-dF)';
     
      % Band-Pass Filter:
      lower_freq = 100;
      upper_freq = 900;
   
      BPF = ((lower_freq < abs(f)) & (abs(f) < upper_freq));
%       figure;
      plot(f,BPF);
  
      time = dt*(0:N-1)';
%       figure;
      plot(time,signal);

   signal=signal-mean(signal);

      spektrum = fftshift(fft(signal))/N;
%       figure;
%       subplot(2,1,1);
      plot(f,abs(spektrum));

     spektrum = BPF.*spektrum.';
%      subplot(2,1,2);
     plot(f,abs(spektrum));

     signal=ifft(ifftshift(spektrum),'symmetric'); %inverse ifft
     figure;
     plot(signal);