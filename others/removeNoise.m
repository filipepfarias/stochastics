function signal_filtered = removeNoise(signal, frequency, passage_frequency, stop_frequency)

	% normalizate frequency
	wp = (passage_frequency/(frequency/2))*pi;
	ws = (stop_frequency/(frequency/2))*pi;

	wt = (ws-wp); 
	wc = (ws+wp)/2;

	M = ceil((6.6*pi)/wt)+1; 

	h = my_lowpass_ideal(wc, M).*hamming(M)';

	signal_filtered = conv(h, signal);

	my_fft(signal_filtered, frequency);
end

