function audio_extracted = removeKnownAudioVoice(audiocomplete , audioremove)
    
   load (audiocomplete);
   load (audioremove);
    
   audiocomplete = Mic1;
   
    audioremove = (y(1:(length(audiocomplete)))).';
    
    sigma_d1 = cov(audiocomplete, audioremove)./var(audioremove);
    sigma_d1 = double( sigma_d1(1,2) );

    audio_extracted = audiocomplete - (sigma_d1*audioremove);

end


