function sigma_d3 = sigma(y,x)

mic1 = x;
mic2 = y;

sigma_d3 = cov(mic2,mic1)./var(mic1);
sigma_d3 = sigma_d3(1,2);

end