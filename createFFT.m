% function [signal] = createFFT(signal, Fs)

% 	% vari�vel N recebe o tamanho do vetor signal
% 	N = length(signal);

% 	% k � um vetor que vai de zero at� N menos 1
% 	k = 0:N-1;                     

% 	% Vetor de tempo N dividido pela frequ�ncia de amostragem     
% 	time = N/Fs;

% 	frequency = k/time;
% 	Fs = length(frequency);

% 	% signal recebe a FFT normalizada do vetor signal sobre N
% 	signal = fftn(signal)/N;   


% 	% cutOff ajusta o eixo signal    
% 	signal_ajust = signal(1:ceil(N/2));

% 	figure();
% 	% Plota a transformada de Fourier e o valor de signal em m�dulo
% 	plot(frequency(1:ceil(N/2)),abs(signal_ajust));
% 	title('Fast Fourier Transform');
% 	xlabel('Frequency (Hz)');
% 	ylabel('Amplitude');
% end


function createFFT(signal, signalTwo, noise, Fs)

	% vari�vel N recebe o tamanho do vetor signal
	N = length(signal);

	% k � um vetor que vai de zero at� N menos 1
	k = 0:N-1;                     

	% Vetor de tempo N dividido pela frequ�ncia de amostragem     
	time = N/Fs;

	frequency = k/time;
	Fs = length(frequency);

	% signal recebe a FFT normalizada do vetor signal sobre N
	signal = fftn(signal)/N;   
	signalTwo = fftn(signalTwo)/N; 	
	noise = fftn(noise)/N; 	

	% cutOff ajusta o eixo signal    
	signal = signal(1:ceil(N/2));
	signalTwo = signalTwo(1:ceil(N/2));
	noise = noise(1:ceil(N/2));
	
	figure();
	% Plota a transformada de Fourier e o valor de signal em m�dulo
	stem(frequency(1:ceil(N/2)),abs(signal));
	title('Fast Fourier Transform - FFT');
	xlabel('Frequ�ncia (Hz)');
	ylabel('Amplitude');

	hold on;
	stem(frequency(1:ceil(N/2)),abs(signalTwo), 'm');
	% stem(frequency(1:ceil(N/2)),abs(noise), 'k');
	legend('Homem','Mulher');

	hold off;
end